<?php

$lang['antiphishing_app_description'] = 'Gateway Antiphishingアプリは、中央のアンチフィッシングエンジンを使用して、ウェブ、FTP、メールなどをスキャンします。エンジンによって検出された悪意のあるリンクからユーザーを保護することで、ネットワークに接続されたデバイスを保護します。';
$lang['antiphishing_app_name'] = 'Gateway Antiphishing';
$lang['antiphishing_app_tooltip'] = 'どんなに精通したインターネット・ユーザーでも、誤ってフィッシングリンクをクリックしてしまうことがあります。 すべてのフィッシング対策機能を有効にしておくことをお勧めします。';
$lang['antiphishing_block_cloaked'] = 'クロークされたURLをブロックする';
$lang['antiphishing_block_ssl_mismatch'] = 'ブロックSSLの不一致';
$lang['antiphishing_heuristics_engine'] = 'ヒューリスティックエンジン';
$lang['antiphishing_signature_engine'] = 'シグネチャーエンジン';
